import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();

id;
title;
Studio;
WeekendIncome;
show;

 
  constructor() { }

toDelete(){
  this.show = false
  this.myButtonClicked.emit(this.title);
}

  ngOnInit() {
    this.id = this.data.id;
    this.title = this.data.title;
    this.Studio = this.data.Studio;
    this.WeekendIncome = this.data.WeekendIncome;
    this.show = true;
  }

}
