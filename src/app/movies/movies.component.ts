import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';

export interface PeriodicElement {
  id: string;
  title: string;
  Studio: string;
  WeekendIncome: string;
  show: string;

}

const movies: PeriodicElement[] = [
  {'id':'0', 'title':'SpiderMan','Studio':'Sony','WeekendIncome':'200','show':'true'},
  {'id':'1', 'title':'ramtry','Studio':'Sony','WeekendIncome':'300','show':'true'},
  {'id':'2', 'title':'title4','Studio':'Sony','WeekendIncome':'300','show':'true'},
  {'id':'3', 'title':'title5','Studio':'Sony','WeekendIncome':'300','show':'true'},
  ];
  

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  text;
  showtext = false
  filterValue = 'true';
  displayedColumns: string[] = ['id', 'title', 'Studio', 'WeekendIncome','show'];
  dataSource = new MatTableDataSource(movies);
   deletes = 'ram'

deleteRow(index){
  movies[index].show = 'false';
  this.deletes = movies[index].title;  
  console.log(movies[index].show);
  this.dataSource.filter = this.filterValue.trim().toLowerCase();
  console.log(this.deletes)
  this.showtext=true
}

showDelete($event){
  this.text = $event;
}

  constructor() { }

  ngOnInit() {
  }

}
